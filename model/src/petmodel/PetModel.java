/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package petmodel;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class PetModel {
    private int HP;
    private int SP;
    private int EP;
    private int lvl;
    private String name;
    private int petType;
    private BufferedImage petImage;
    private FileWriter fw;
    
    public PetModel(String petFile){
        Path file = Paths.get("./" + petFile);
        this.HP = 50;
        this.SP = 30;
        this.EP = 20;
        this.lvl = 0;
        this.name = petFile.substring(5);
        
        initializeModel(file);
    }
    
    private boolean initializeModel(Path file){
      boolean flagOK = true;
      String line = null;
      
      Charset charset = Charset.forName("UTF-8");
      try(BufferedReader reader = Files.newBufferedReader(file, charset )) {
          while ((line = reader.readLine()) != null) {
              System.out.println(line);
          }
      } catch (IOException x) {
            flagOK = false;
            
            if(x.getClass().getName().equals("java.nio.file.NoSuchFileException")){
                FileWriter testfile;
                try{
                    testfile = new FileWriter ("sample/sample.txt", true);
                    testfile.write("HP:" + this.HP);
                    testfile.write("SP: "+this.SP);
                    testfile.write("EP: " + this.EP);
                    testfile.write("lvl: " +this.lvl);
                    testfile.write("name: " + this.name);
                    testfile.close();
                } catch (IOException ex) {
                    Logger.getLogger(PetModel.class.getName()).log(Level.SEVERE, null, ex);
                }
                flagOK = createFile(file);
                    
                }
            }
        System.out.println(line);
        return flagOK;
        }

    private boolean createFile(Path file) {
        boolean flagOK = true;
        
        Charset charset = Charset.forName("UTF-8");
        String s = "HP:" + this.HP + "SP: " +this.SP+ "EP:"+this.EP+"lvl: "+ this.lvl + "name: "+this.name;
        try
           (BufferedWriter writer = Files.newBufferedWriter(file, charset)) {
            
        } catch (IOException x) {
            System.err.format("Create IOException: %s%n", x);
            flagOK =false;
            
        }
        return flagOK;
        
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public void setSP(int SP) {
        this.SP = SP;
    }

    public void setEP(int EP) {
        this.EP = EP;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPetType(int petType) {
        this.petType = petType;
    }

    public void setPetImage(BufferedImage petImage) {
        this.petImage = petImage;
    }

    public void setFw(FileWriter fw) {
        this.fw = fw;
    }

    public int getHP() {
        return HP;
    }

    public int getSP() {
        return SP;
    }

    public int getEP() {
        return EP;
    }

    public int getLvl() {
        return lvl;
    }

    public String getName() {
        return name;
    }

    public int getPetType() {
        return petType;
    }

    public BufferedImage getPetImage() {
        return petImage;
    }

    public FileWriter getFw() {
        return fw;
    }
    }

   



